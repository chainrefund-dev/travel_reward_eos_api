const express = require('express');
const router = express.Router();
const app = express();

const transaction = require("../controller/eosnetwork.transaction");
const txs_ca = require("../controller/caserver.transaction");
const get_eos = require("../controller/eosnetwork.getTx");
const check_certkey = require("../utils/certtificate/certkey").check_certkey;

const logger = require("../utils/format/format.logger").logger;


router.post('/v1.0/enroll_user', check_certkey, (req, res) => {
    transaction.enroll_user(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/buy_rams', check_certkey, (req, res) => {
    transaction.buy_rams(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/stake_resource', check_certkey, (req, res) => {
    transaction.stake_resource(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/user_action', check_certkey, (req, res) => {
    transaction.user_action(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/create_token', check_certkey, (req, res) => {
    transaction.create_token(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/reward_token', check_certkey, (req, res) => {
    transaction.reward_token(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/send_token', check_certkey, (req, res) => {
    transaction.send_token(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/allow_transfer', check_certkey, (req, res) => {
    transaction.allow_transfer(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.post('/v1.0/freeze_token', check_certkey, (req, res) => {
    transaction.freeze_token(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.get('/v1.0/get_account', check_certkey, (req, res) => {
    get_eos.get_account(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.get('/v1.0/get_total_currency', check_certkey, (req, res) => {
    get_eos.get_total_currency(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.get('/v1.0/get_accounts_cnt', check_certkey, (req, res) => {
    get_eos.get_accounts_cnt(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

router.get('/v1.0/get_transaction_cnt', check_certkey, (req, res) => {
    get_eos.get_transaction_cnt(req, logger).then((result) => {
        res.send(result);
    })
    .catch( error => {
        res.send(error);
    });
});

module.exports = router;