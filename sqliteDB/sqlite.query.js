let create_tb_key_store = () => {
    let query = `
        CREATE TABLE tb_key_store(
            nid             BIGINT      PRIMARY KEY,
            type            TINYINT     DEFAULT 1,
            status          TINYINT     DEFAULT 1,
            account_name    VARCHAR(12) NOT NULL, 
            owner_pri       VARCHAR(80) NOT NULL, 
            owner_pub       VARCHAR(80) NOT NULL,
            active_pri      VARCHAR(80) NOT NULL, 
            active_pub      VARCHAR(80) NOT NULL,
            date_reg        DATE,
            date_last       DATE
        )
        ;`;
    return query;
}

let drop_table = (table) => {
    let query = `
            DROP TABLE ${table};
        `
    return query;
}

let enroll_user = (datas) => {
    let query = `
        INSERT INTO tb_key_store(nid, type, account_name, owner_pri, owner_pub, active_pri, active_pub, date_reg, date_last) 
            VALUES(
                ${datas.nid}, 
                2, 
                "${datas.account}", 
                "${datas.owner_pri}", 
                "${datas.owner_pub}", 
                "${datas.active_pri}", 
                "${datas.active_pub}", 
                "${datas.date_reg}",
                "${datas.date_last}"
            )
        ;`;
    return query;
}

let get_user = (datas) => {
    
    let query = `
        SELECT account_name, active_pri FROM tb_key_store
        `
        if(datas.nid.length > 1){
            query += `WHERE nid = "${datas.nid}"`
        }else{
            query += `WHERE eos_type = "${datas.nid}"`
        }
        query += `;`;
    return query;
}

let get_from_to_users = (datas) => {
    
    let query = `
        SELECT  a.active_pri    AS from_key, 
                a.account_name  As from_acc, 
                b.active_pri    AS to_key, 
                b.account_name  AS to_acc
        FROM`
        if(datas.from_acc.length > 1){
            query += `(select nid, active_pri, account_name from tb_key_store where nid = "${datas.from_acc}") a,`
        }else{
            query += `(select eos_type, active_pri, account_name from tb_key_store where eos_type = "${datas.from_acc}") a,`
        }
            query += `(select nid, active_pri, account_name from tb_key_store where nid = "${datas.to_acc}") b`
        query += ` WHERE`
        if(datas.from_acc.length > 1){
            query += ` a.nid = "${datas.from_acc}"`
        }else{
            query += ` a.eos_type = "${datas.from_acc}"`
        }
            query += ` OR`
            query += ` b.nid = "${datas.to_acc}"`
            query += `;`
    return query;
}

let get_users_key = () => {
    let query = `
        SELECT active_pri FROM tb_key_store
        ;`;
    return query;
}

let update_user_status = (datas) => {

    let query = `
        UPDATE tb_key_store 
        SET     status      =       "${datas.status}",
                date_last   =       "${datas.date}
        WHERE   nid         =       "${datas.nid}"
        ;`;
    return query;
}

let update_user_date = (datas) => {
    let query = `
        UPDATE tb_key_store
        SET     date_last   =       "${datas.date}"
        WHERE   nid         =       "${datas.nid}"
        ;`;
    return query;
}

let enroll_admin = (datas) => {
    let query = `
        INSERT INTO tb_key_store(nid, type, account_name, owner_pri, owner_pub, active_pri, active_pub, date_reg, date_last)
            VALUES(
                ${datas.nid},
                ${datas.type}, 
                "${datas.account}", 
                "${datas.owner_pri}", 
                "${datas.owner_pub}", 
                "${datas.active_pri}", 
                "${datas.active_pub}", 
                "${datas.date_reg}",
                "${datas.date_last}"
            )
        ;`;
    return query;    
}

let check_completion = (datas) => {
    let query = `
        SELECT completion, account_name 
        FROM   tb_key_store
        WHERE  nid = ${datas.nid} 
        ;`
    return query;
}

let update_completion = (datas) => {
    let query = `
        UPDATE tb_key_store
        SET    completion = ${datas.completion},
               nid = ${datas.nid}
        WHERE  nid = ${datas.nid}
        ;`
    return query;
}

let change_user = (datas) => {
    let query = `
        UPDATE tb_key_store
        SET    type = 2,
               account_name = "${datas.account}",
               owner_pri = "${datas.owner_pri}",
               owner_pub = "${datas.owner_pub}",
               active_pri = "${datas.active_pri}",
               active_pub = "${datas.active_pub}",
               date_reg = "${datas.date_reg}",
               date_last = "${datas.date_last}"
        WHERE nid = ${datas.nid}
        ;`
    return query;
}

let commit = () => {
    let query = `
        commit;
    `
    return query;
}

module.exports = {
    create_tb_key_store,
    drop_table,
    enroll_user,
    get_user,
    get_users_key,
    get_from_to_users,
    update_user_status,
    update_user_date,
    enroll_admin,
    check_completion,
    update_completion,
    change_user
}