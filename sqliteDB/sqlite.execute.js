const { create_tb_key_store, drop_table, enroll_user, get_user, get_from_to_users, get_users_key, update_user_date, check_completion, update_completion, change_user } = require("./sqlite.query");
const conn = require("./sqlite.connection").conn;
const encrypt_pass = require("../utils/certtificate/cipher").encrypt_pass;
const CIPHER = require("../constants/contants").CIPHER;

class SqlQueryObj{

    constructor(){
        this.conn = conn;
    }

    create_tb_key_store(){
        let query = create_tb_key_store();
        this.conn.all(query, (err) => {
            if(err){
                console.log(err);
                reject(err);
            }else{
                console.log(this.conn.open);
                resolve(this.conn.open)
            }
        });
    }

    drop_table(table){
        let query = drop_table();
        this.conn.all(query, (err, rows) => {
            if(err){
                console.log("err: " + err);
            }else{
                console.log("rows: " + rows);
            }
        });
    }

    enroll_user(nid, account, own_pri, own_pub, act_pri, act_pub, date){

        let datas = {
            nid: nid,
            account: account,
            owner_pri: encrypt_pass(1, own_pri),
            owner_pub: encrypt_pass(1, own_pub),
            active_pri: encrypt_pass(1, act_pri),
            active_pub: encrypt_pass(1, act_pub),
            date_reg: date,
            date_last: date
        }
        
        return new Promise((resolve, reject) => {
            let query = enroll_user(datas);
            this.conn.run(query, (err) => {
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(this.conn.open)
                }
            });
        });
    }

    get_user(nid){
        
        let datas = {
            nid: nid
        }
        
        return new Promise((resolve, reject) => {
            let query = get_user(datas);
            this.conn.all(query, (err, rows) => {
                if(err){
                    reject(err);
                }else{
                    if(rows.length === 0){
                        reject("no such colomn : []");
                    }
                    resolve(rows);
                }
            });
        });
    }
    
    get_from_to_users(from_acc, to_acc){
        
        let datas = {
            from_acc: from_acc, 
            to_acc: to_acc
        }

        return new Promise((resolve, reject) => {
            let query = get_from_to_users(datas);
            this.conn.all(query, (err, rows) => {
                if(err){
                    reject(err);
                }else{
                    if(rows.length === 0){
                        new Error({"error" :"no such colomn : []"});
                        // reject();
                    }
                    resolve(rows[0])
                }
            });
        });
    }
    
    get_users_key(){
        
        return new Promise((resolve, reject) => {
            let query = get_users_key();
            this.conn.all(query, (err, rows) => {
                if(err){
                    reject(err);
                }else{
                    if(rows.length === 0){
                        reject({"error" :"no such colomn : []"});
                    }
                    resolve(rows);
                }
            });
        });
    }
    
    update_user_status(nid, status, date){
        
        let datas = {
            nid: nid,
            status: status,
            date: date
        }
        
        console.log(account);
        console.log(date);
        return new Promise((resolve, reject) => {
            let query = update_user_status(datas);
            this.conn.run(query, (err) => {
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(this.conn.open)
                }
            });
        });
    }
    
    update_user_date(nid, date){
        
        let datas = {
            nid: nid,
            date: date
        }
        
        return new Promise((resolve, reject) => {
            let query = update_user_date(datas);
            this.conn.run(query, (err) => {
                if(err){
                    reject(err);
                }else{
                    resolve(this.conn.open)
                }
            });
        });
    }

    enroll_admin(nid, account, pri, pub, date){

        let datas = {
            nid: nid,
            type: 4,
            account: account,
            owner_pri: pri,
            owner_pub: pub,
            active_pri: pri,
            active_pub: pub
        }

        return new Promise((resolve, reject) => {
            let query = enroll_admin(datas);
            this.conn.all(query, (err) => {
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    console.log(this.conn.open);
                    resolve(this.conn.open)
                }
            });
        });
    }

    check_completion(nid){

        let datas = {
            nid: nid
        }

        return new Promise((resolve, reject) => {
            let query = check_completion(datas);
            this.conn.all(query, (err, rows) => {
                if(err){
                    reject(err);
                }else{
                    if(rows.length === 0){
                        resolve(rows);
                    }
                    resolve(rows);
                }
            });
        });
    }

    update_completion(nid, completion){

        let datas = {
            nid: nid,
            completion: completion
        }

        return new Promise((resolve, reject) => {
            let query = update_completion(datas);
            this.conn.run(query, (err) => {
                if(err){
                    reject(err);
                }else{
                    resolve(this.conn.open)
                }
            });
        });
    }

    change_user(nid, account, own_pri, own_pub, act_pri, act_pub, date){

        let datas = {
            nid: nid,
            account: account,
            owner_pri: encrypt_pass(1, own_pri),
            owner_pub: encrypt_pass(1, own_pub),
            active_pri: encrypt_pass(1, act_pri),
            active_pub: encrypt_pass(1, act_pub),
            date_reg: date,
            date_last: date
        }
        
        return new Promise((resolve, reject) => {
            let query = change_user(datas);
            this.conn.run(query, (err) => {
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    resolve(this.conn.open)
                }
            });
        });
    }
}

module.exports = {
    SqlQueryObj
}