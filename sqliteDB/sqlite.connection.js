const sqlite = require("sqlite3");
const path = require('path');
const database_env = require("../constants/contants").DATABASE_PATH;
let dbPath = "";

switch(process.env.SERVER){
    case "live":
        dbPath = path.resolve(database_env.PATH);
        break;
    case "test":
        dbPath = path.resolve(database_env.PATH);
        break;
    case "dev":
        dbPath = path.resolve(__dirname, database_env.PATH);
        break;
    default:
        break;

}
const conn = new sqlite.Database(dbPath);

module.exports = {
    conn
}
