const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');      // development only
const fetch = require('node-fetch');                                    // node only; not needed in browsers
const { TextEncoder, TextDecoder } = require('util');
const { res_format, res_error } = require("../utils/format/format.response");

const { BUY, ACC, NODE, MODEL } = require("../constants/contants");
const { EosManager } = require("../utils/eosnetwork/eosnetwork.manager");
const { tx_enroll_user, tx_rams, tx_stake } = require("../utils/format/format.transaction");


// const signatureProvider = new JsSignatureProvider([ACC.TOKEN.ACTIVE.PRI, ACC.USER1.ACTIVE.PRI, ACC.USER2.ACTIVE.PRI]);
const rpc = new JsonRpc("https://eos.greymass.com", { fetch });
// const rpc = new JsonRpc("https://api-kylin.eoslaomao.com", { fetch });
// const eos = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder("utf-8"), textEncoder: new TextEncoder()});

rpc.get_account("trofvhrqn2np").then(res => {
  console.log("####################")
  console.log(res)
  if (res.account_name) {
    console.log("있어")
  } else {
    console.log("없어")
  }

  console.log(res.account_name);
}).catch(err => {
  console.log("없어")
})

let enroll_admin = () => {
  return new Promise((resolve, reject) => {
    const eosManager = new EosManager();
    eosManager.gen_keys().then(keys => {

      let pri_key = keys.pri_key;
      let pub_key = keys.pub_key;

      console.log("pri_key")
      console.log("pub_key")
      console.log(pri_key)
      console.log(pub_key)

      let users_key = [];
      users_key.push("PW5JcKEHU2MM2x1Fitb7gVBNk8B1EmLQTMRUPjgnkkwe2kiJWJe15");
      eosManager.set_cached_key(users_key);
      let eos = eosManager.eos;

      eos.transact({
        actions: [
          tx_enroll_user("travelmgracc", pub_key, pub_key),
          tx_rams(BUY.BUY, ACC.TOKEN.NAME, "travelmgracc"),
          tx_stake(BUY.BUY, ACC.TOKEN.NAME, "travelmgracc")
        ]
      },
        {
          blocksBehind: 3,
          expireSeconds: 30,
        }).then(res => {
          console.log("create account");
          console.log(res);
        }).catch(error => {
          console.log("catch:  ", error);
          reject(res_error(error.json, 2));
        });
    });
  })
};

// enroll_admin()