#include <eosio/eosio.hpp>
#include <eosio/name.hpp>

#include <string>

class [[eosio::contract]] userAction : public eosio::contract {

   public:
      using eosio::contract::contract;

      [[eosio::action]]
      void airticket( eosio::name user, 
		      std::string action_type, 
		      std::string serial,
		      std::string time_stamp, 
		      std::string reward_amount ) {
         eosio::check(is_account(_self), "this account is not in eos network");
      }

      [[eosio::action]]
      void logment( eosio::name user,
                    std::string action_type,
		              std::string serial,
                    std::string time_stamp,
                    std::string reward_amount ) {

         eosio::check(is_account(_self), "this account is not in eos network");
      }

      [[eosio::action]]
      void refundticket( eosio::name user,
                         std::string action_type,
		                   std::string serial,
                         std::string time_stamp,
                         std::string reward_amount ) {

         eosio::check(is_account(_self), "this account is not in eos network");
      }

};
EOSIO_DISPATCH(userAction, (airticket)(logment)(refundticket))

