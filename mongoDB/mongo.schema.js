const mongoose = require("mongoose");

const EOS_NEW_ACCOUNT = mongoose.Schema({
    "id": String,
    "block_num": Number,
    "block_time": Date,
    "producer_block_id": Number,
    "receipt": {
        "status": String,
        "cpu_usage_us": Number,
        "net_usage_words": Number
    },
    "elapsed": Number,
    "net_usage": Number,
    "scheduled": Boolean,
    "action_traces": [
        {
            "action_ordinal": Number,
            "creator_action_ordinal": Number,
            "closest_unnotified_ancestor_action_ordinal": Number,
            "receipt": {
                "receiver": String,
                "act_digest": String,
                "global_sequence": Number,
                "recv_sequence": Number,
                "auth_sequence": [
                    [
                        String,
                        Number
                    ]
                ],
                "code_sequence": Number,
                "abi_sequence": Number
            },
            "receiver": String,
            "act": {
                "account": String,
                "name": String,
                "authorization": [
                    {
                        "actor": String,
                        "permission": String
                    }
                ],
                "data": {
                    "from": String,
                    "to": String,
                    "quantity": String,
                    "memo": String
                },
                "hex_data": String
            },
            "context_free": Boolean,
            "elapsed": Number,
            "console": String,
            "trx_id": String,
            "block_num": Number,
            "block_time": Date,
            "producer_block_id": Number,
            "account_ram_deltas": [
                {
                    "account": String,
                    "delta": Number
                }
            ],
            "except": String,
            "error_code": String,
            "inline_traces": [
                {
                    "action_ordinal": Number,
                    "creator_action_ordinal": Number,
                    "closest_unnotified_ancestor_action_ordinal": Number,
                    "receipt": {
                        "receiver": String,
                        "act_digest": String,
                        "global_sequence": Number,
                        "recv_sequence": Number,
                        "auth_sequence": [
                            [
                                String,
                                Number
                            ]
                        ],
                        "code_sequence": Number,
                        "abi_sequence": Number
                    },
                    "receiver": String,
                    "act": {
                        "account": String,
                        "name": String,
                        "authorization": [
                            {
                                "actor": String,
                                "permission": String
                            }
                        ],
                        "data": {
                            "from": String,
                            "to": String,
                            "quantity": String,
                            "memo": String
                        },
                        "hex_data": String
                    },
                    "context_free": Boolean,
                    "elapsed": Number,
                    "console": String,
                    "trx_id": String,
                    "block_num": Number,
                    "block_time": Date,
                    "producer_block_id": Number,
                    "account_ram_deltas": [],
                    "except": String,
                    "error_code": Number,
                    "inline_traces": []
                }
            ]
        }
    ],
    "account_ram_delta": Number,
    "except": String,
    "error_code": Number
});

module.exports = {
    EOS_NEW_ACCOUNT
}