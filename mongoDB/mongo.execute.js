const mongoose = require("mongoose");
// mongoose.connect("mongodb://localhost/test", {useNewUrlParser: true});
const db = mongoose.Connection;
const { EOS_NEW_ACCOUNT } = require("./mongo.schema");

let schema_model = null;
const user = "";
const pwd = "";
const db_url = "localhost:27017"
const database = "chainrefund";

let initDBConnection = () => {
    // mongoose.Promise = global.Promise;
    return new Promise((resolve, reject) => {
        mongoose.connect(
            `mongodb://${user}:${pwd}@${db_url}/${database}`,
            { useNewUrlParser: true },
        ).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
}

let putTxData = (txData, model) => {
    return new Promise((resolve, reject) => {
        console.log(model)
        schema_model = mongoose.model(model, EOS_NEW_ACCOUNT, model);
        initDBConnection().then( res => {
            schema_model.insertMany(txData).then( res => {
                resolve(res);
            });
        }).catch( err => {
            reject(err);
        });
    });
}

module.exports = {
    putTxData
}
