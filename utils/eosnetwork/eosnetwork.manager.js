const { Api, JsonRpc } = require('eosjs');
const { TextEncoder, TextDecoder } = require('util');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');
const ecc = require('eosjs-ecc');

const NODE = require("../../constants/contants").NODE;
const fetch = require('node-fetch');

class EosManager {
    
    constructor(){
        this.rpc = new JsonRpc(NODE, { fetch });
    }

    set_cached_key(keys) {
        this.signatureProvider = new JsSignatureProvider(keys);
        this.eos = new Api({ rpc: this.rpc, signatureProvider: this.signatureProvider, textDecoder: new TextDecoder("utf-8"), textEncoder: new TextEncoder() });
    }
    
    get_eos_rpc(){
        return this.eos;
    }

    gen_keys() {
        let keys = {};
        return new Promise((resolve, reject) => {
            ecc.randomKey().then(privateKey => {
                keys.pri_key = privateKey;
                keys.pub_key = ecc.privateToPublic(privateKey);
                return keys;
            }).then(key => {
                resolve(key);
            }).catch(error => {
                reject(error);
            })
        });

    }

    gen_account() {
        let chars = "12345abcdefghiklmnopqrstuvwxyz";
        let string_length = 10;
        let eos_account = 'tr';
        for (let i = 0; i < string_length; i++) {
            let rnum = Math.floor(Math.random() * chars.length);
            eos_account += chars.substring(rnum, rnum + 1);
        }

        return eos_account;
    }
}

module.exports = {
    EosManager
}
