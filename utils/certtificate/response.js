const { res_format, res_error } = require("../format/format.response");
const { SUCCESS, ERROR, MESSAGE } = require("../../constants/contants");

exports.sendResponseFailDataNid = function(req, res) {
    try {
        let result = {};
        let err_details = "";
        if( !req.decoded_err)
        {
            result = res_error(err_details, ERROR.TYPE.FORBIDDEN.CODE);
        }
        else
        {
            result = res_error(err_details, ERROR.TYPE.TOKEN_INVALID.CODE);
        }

        return res.send(result);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

exports.sendResponseFailDataNotNId = function(req, res) {
    try {
        let result;
        let err_details = "";
        result = res_error(err_details, ERROR.TYPE.NID_INVALID.CODE);

        return res.send(result);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};
