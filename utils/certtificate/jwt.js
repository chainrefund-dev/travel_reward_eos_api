const jwt = require('jsonwebtoken');
const responseCommon = require('./response');
const logger = require("../format/format.logger").logger;
const { MESSAGE, ERROR, CERT_LEVEL } = require("../../constants/contants");

const PRIVATE_KEY = 'rlackdtn19$(06@^rlarn';
const PRIVATE_KEY_CERT = 'dltnstls15$%dugocndan15(*';
const ACCESS_TOKEN = 'access_token';
// const CERT_TOKEN = 'cert_token';
const CERT_TOKEN = 'cert-token';

exports.verifyToken_cert = function(req, res, next) {

    let certToken = "";
    let private_key = "";

    if(CERT_LEVEL.LEVEL == "ACCESS"){
        certToken = req.headers[ACCESS_TOKEN];
        private_key = PRIVATE_KEY;
    }else if(CERT_LEVEL.LEVEL == "CERT"){
        certToken = req.headers[CERT_TOKEN]
        private_key = PRIVATE_KEY;
    }

    logger.log("info", `CERT TOKEN: ${certToken != null}`);

    if(certToken === undefined) {
        responseCommon.sendResponseFailDataNid(req, res);
        return;
    }

    if(certToken === '') {
        responseCommon.sendResponseFailDataNid(req, res);
        return;
    }

    jwt.verify(certToken, private_key, function(err, decoded) {
        if(err != null) {
            req.decoded_err = err;
            responseCommon.sendResponseFailDataNotNId(req, res);
            return;
        }
        req.decoded = decoded;

        let nid = 0;
        if(req.method === 'GET') {
            nid = req.query.nid;
        }
        else {
            nid = req.body.nid;
        }

        if(parseInt(nid) !== req.decoded._nid) {
            responseCommon.sendResponseFailDataNotNId(req, res);
            return;
        }
        else {
            next();
        }
    })
};
