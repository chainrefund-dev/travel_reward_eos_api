#!/bin/bash
DOCKER_CONTAINER_NAME="travelre.api.eos"
DOCKER_IMAGE_NAME="travelre.api.eos/nodejs"
DOCKER_IMAGE_TAG="v1.0.0"
COMPOSE_FILE=docker-compose.yaml
echo "start TRAVELRE_EOS_API Server"

echo "Stop and Remove current ${DOCKER_CONTAINER_NAME} docker container..."
docker rm -f $(docker ps -aqf name="${DOCKER_CONTAINER_NAME}")
echo "Remove ${DOCKER_CONTAINER_NAME} docker IMAGE!!.."
docker rmi -f ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}
echo "Build ${DOCKER_CONTAINER_NAME} SERVER image.."
docker build -t ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} .
docker-compose -f ${COMPOSE_FILE} up -d 2>&1


